package main

import (
	"flag"
	"html/template"
	"io/ioutil"
	"net/http"	
	"log"
	"net"
	"regexp"
)

var (
	addr = flag.Bool("addr", false, "find open address and print to final-port.txt")
)

type Page struct {
	Title string
	Body []byte
}

// It complains (and exits the program) when there is no edit.html or view.html
var tmpl_path = "./tmpl/"
var templates = template.Must(template.ParseFiles(tmpl_path + "edit.html", tmpl_path + "view.html"))
var data_path = "./data/"

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	// tmpl is just the template filename
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (p *Page) save() error {
	// p is a pointer to a page object
	filename := data_path + p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := data_path + title + ".txt"
	body, err := ioutil.ReadFile(filename)
	// check for error if not nil
	if err != nil {		
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil // return a Page instance
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title) // declaration needs := as one needs to determine the type of the variable
	if err != nil {
		p = &Page{Title : title} // subsequent assignments needs = only
	}	
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {		
		m := validPath.FindStringSubmatch(r.URL.Path)
		// nil means no matches found
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func main() {
	// It has the JS style: passing in a function
	flag.Parse()
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))

	if *addr {
		l, err := net.Listen("tcp", "127.0.0.1:0")
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile("final-port.txt", []byte(l.Addr().String()), 0644)
		if err != nil {
			log.Fatal(err)
		}
		s := &http.Server{}
		s.Serve(l)
		return
	}

	http.ListenAndServe(":8080", nil)
}